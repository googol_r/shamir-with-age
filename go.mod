module github.com/dennis-tra/shamir

go 1.22.5

toolchain go1.23.6

require (
	filippo.io/age v1.2.0
	github.com/hashicorp/vault v1.17.6
)

require (
	golang.org/x/crypto v0.27.0 // indirect
	golang.org/x/sys v0.25.0 // indirect
)
