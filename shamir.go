package main

import (
	"encoding/base64"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"filippo.io/age"
	"filippo.io/age/armor"
	"github.com/hashicorp/vault/shamir"
)

type recipientsFlags []age.Recipient

func (i *recipientsFlags) String() string {
	return ""
}

func (i *recipientsFlags) Set(value string) error {
	if value == "" {
		return nil
	} else if strings.HasPrefix(value, "@") {
		reader, err := os.Open(strings.TrimPrefix(value, "@"))
		if err != nil {
			return err
		}
		defer reader.Close()

		recipient, err := age.ParseRecipients(reader)
		if err != nil {
			return err
		}
		*i = append(*i, recipient...)
		return nil
	} else {
		reader := strings.NewReader(value)
		recipient, err := age.ParseRecipients(reader)
		if err != nil {
			return err
		}
		*i = append(*i, recipient...)
		return nil
	}
}

func main() {
	splitCmd := flag.NewFlagSet("split", flag.ExitOnError)
	shares := splitCmd.Int("shares", 5, "Number of shares to split into")
	threshold := splitCmd.Int("threshold", 3, "Number of shares needed to restore")
	outdir := splitCmd.String("outdir", "-", "The directory to put the outputs in")
	recipients := recipientsFlags{}
	splitCmd.Var(&recipients, "recipient", "Specify the age public keys of the recipients of the shares")

	restoreCmd := flag.NewFlagSet("restore", flag.ExitOnError)

	if len(os.Args) < 2 {
		fmt.Println("expected 'split' or 'restore' subcommands")
		os.Exit(1)
	}

	var err error
	switch os.Args[1] {
	case "split":
		err = splitCmd.Parse(os.Args[2:])
		if err != nil {
			break
		}
		err = split(splitCmd.Arg(0), *shares, *threshold, recipients, *outdir)
	case "restore":
		err = restoreCmd.Parse(os.Args[2:])
		if err != nil {
			break
		}
		err = restore(restoreCmd.Arg(0))
	default:
		err = fmt.Errorf("expected 'split' or 'restore' subcommands")
	}

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func split(filename string, shares int, threshold int, recipients recipientsFlags, outdir string) error {
	if shares != len(recipients) {
		return fmt.Errorf("expected as many recipients as shares, shares = %d, recipients = %d", shares, len(recipients))
	}

	src, err := source(filename)
	if err != nil {
		return err
	}
	defer src.Close()

	secret, err := ioutil.ReadAll(src)
	if err != nil {
		return err
	}

	byteShares, err := shamir.Split(secret, shares, threshold)
	if err != nil {
		return err
	}

	if outdir == "-" {
		for index, byteShare := range byteShares {
			encodedShare := base64.StdEncoding.EncodeToString(byteShare)
			fmt.Println("# Share", index)
			fmt.Println("## age encrypted")
			a := armor.NewWriter(os.Stdout)
			w, err := age.Encrypt(a, recipients[index])
			if err != nil {
				return err
			}
			if _, err := w.Write([]byte(encodedShare)); err != nil {
				return err
			}
			if err := w.Close(); err != nil {
				return err
			}
			if err := a.Close(); err != nil {
				return err
			}

			fmt.Println()
		}
	} else {
		abs, err := filepath.Abs(outdir)
		if err != nil {
			return err
		}
		if err := os.MkdirAll(abs, os.ModePerm); err != nil {
			return err
		}

		clearDirectoryContents(abs)

		for index, byteShare := range byteShares {
			encodedShare := base64.StdEncoding.EncodeToString(byteShare)
			encodedFile, err := os.Create(filepath.Join(abs, fmt.Sprint("share", index, "_base64.txt")))
			if err != nil {
				return err
			}
			defer encodedFile.Close()

			encodedFile.Write([]byte(encodedShare))

			encryptedFile, err := os.Create(filepath.Join(abs, fmt.Sprint("share", index, "_age.txt")))
			if err != nil {
				return err
			}
			defer encryptedFile.Close()

			a := armor.NewWriter(encryptedFile)
			w, err := age.Encrypt(a, recipients[index])
			if err != nil {
				return err
			}
			if _, err := w.Write([]byte(encodedShare)); err != nil {
				return err
			}
			if err := w.Close(); err != nil {
				return err
			}
			if err := a.Close(); err != nil {
				return err
			}
		}
	}

	return nil
}

func clearDirectoryContents(directory string) error {
	// Open the directory and read all its files.
	dirRead, err := os.Open(directory)
	if err != nil {
		return err
	}
	dirFiles, err := dirRead.Readdir(0)
	if err != nil {
		return err
	}

	// Loop over the directory's files.
	for index := range dirFiles {
		fileHere := dirFiles[index]

		// Get name of file and its full path.
		nameHere := fileHere.Name()
		fullPath := filepath.Join(directory, nameHere)

		// Remove the file.
		os.RemoveAll(fullPath)
	}
	return nil
}

func restore(filename string) error {
	src, err := source(filename)
	if err != nil {
		return err
	}
	defer src.Close()

	data, err := ioutil.ReadAll(src)
	if err != nil {
		return err
	}
	strShares := strings.Split(string(data), "\n")

	byteShares := [][]byte{}
	for _, strShare := range strShares {
		if strShare == "" {
			continue
		}

		byteShare, err := base64.StdEncoding.DecodeString(strShare)
		if err != nil {
			return err
		}

		byteShares = append(byteShares, byteShare)
	}

	secret, err := shamir.Combine(byteShares)
	if err != nil {
		return err
	}

	fmt.Println(string(secret))

	return nil
}

func source(filename string) (io.ReadCloser, error) {
	if filename == "" {
		stat, err := os.Stdin.Stat()
		if err != nil {
			return nil, fmt.Errorf("you have an error in stdin: %w", err)
		}

		if (stat.Mode() & os.ModeNamedPipe) == 0 {
			return nil, fmt.Errorf("nothing to read from stdin")
		}

		return os.Stdin, nil
	}

	f, err := os.Open(filename)
	if err != nil {
		return nil, fmt.Errorf("error opening file %q: %w", filename, err)
	}
	return f, nil
}
